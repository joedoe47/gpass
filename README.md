# gpass

## About 

This isn't a fork of the pass app. I just got an idea to use/make something similar to pass thats easier and a bit more flexible for me to use.

it is a wrapper for gpg and uses (pgp)[https://ssd.eff.org/en/glossary/pgp] to encrypt data and it defaults to AES256 and uses sha512 to verify the file.

You will want to try this if:

- you want a good and simple password manager

- you change passwords often

- you want to generate random passwords, passphrases from word lists, or even hash based passwords 

- you want to have more control of your passwords or data and they are secured 

- you want to leak less metadata about your passwords compared to pass

- you plan to use this on multiple linux, unix or linux/unix-like systems (tested mostly on termux, fedora, and debian but I focused on using coreutils and gpg)

Pass already exists but this is something I thought would be cool to make anyways.

This leaks less metadata because its a script that uses GPG to store multiple passwords into a file in CSV format. Where as pass will store 1 password per file and you organize these passwords using folders. Thus allowing an adversary to know that you have an account on a specific site, eg. 'facebook'. However with gpass I could have a file called "social-media.gpg" and I may or may not have my facebook accounts there.

Please do not misunderstand, pass isn't insecure or flawed in anyway; both pass and gpass, at worst case scenario an attacker knows how big an encrypted file is, what might be inside based on the name, and when it changed but not necessarily what changed. Both also use GPG. 

I made gpass to lessen the amount of data an adversary can get from the name of a file (via security through obscurity), the ability to use a password or a GPG ID, and 2 other forms of password generation. 

The main difference with gpass is that with gpass, you have 1 file with multiple passwords. An adversary would have a harder time figuring out what passwords are in file "X.gpg", should they somehow grab a hold of your LUKS drive or see your git server.

Naturally the larger a password file the slower sed is to find/add/delete data but you would probably need around a 900,000 passwords on a raspberry pi 3 to really notice slow downs but performance will vary depending on your hardware and what its doing.

This can be as secure or as relaxed as you want it, depending on how you use it!

## How to try/install
Clone:
https://git.joepcs.com/porjects/gpass
https://github.com/orien3243/gpass
https://gitlab.com/joedoe47/gpass

To try:

    $ bash /path/to/gpass [arguments]

if you want something more permanent you can just use an alias: 

    $ cd gpass && echo "alias gpass=\"$(pwd)/gpass\"" >> "$HOME/.bashrc"

every so often just do "git pull" in this directory every so often to make sure it stays up to date. 

## Configuration

There are some configuration options that can be set via evnironment variables to tweak how gpass operates. you can add these variables to your .bashrc or .profile to properly make gpass work to your liking.

- I want to use another encryption algorithm

        - $ export ALGORITHM="twofish" (or whichever algorithm you fancy; camilla256 is another)

- If you think /dev/random is better.

        - $ export ENTROPY_SOURCE="/dev/random"


## Backwards compatibility 

I have tested gpass in a few environments. Debian, Fedora, Archlinux, Termux (no proot). Because these are the linux and linux-like environments I use the most. So rest assured it will work on linux mint, windows new ubuntu bash, and any operating system so long as it has gpg, bash, and coreutils.  

## I hate this program!

Since the program uses a simple CSV standard and uses gpg, you can switch to any alternate method of managing your passwords if you think this program isn't for you. (using a terminal only method for passwords can be an issue, I've tried to get my freinds to use this to no avail)

I hate the concept of being locked down so CSV seemed like a good choice. There are a ton of converters from CSV to keepass, lastpass, 1pass, pass, etc.

## Suggested Security Practices

- Only use machines you trust. I would not suggest you open a password file in an environment you do not trust or is capable of snooping on you or your data.

- Use EFF's [recommendations](https://ssd.eff.org/en/module/creating-strong-passwords) regarding passwords and what not (use dice ware, use 2 factor auth, use fake answers for security questions, be carefule how you sync the encrypted contents, etc.)

- Use [LUKS](https://gitlab.com/cryptsetup/cryptsetup/) drives to store your data (security is best when layered; keep a back up of the passphrase for this somewhere safe too or do not.)

	- if you are on IOS or Android use full disk encryption but note that phones are *RARELY* secure environments to be working on. (yes even if 3rd party roms)

- Try to use NTFS or FAT to store files (journaled file systems can have files restored if its done in a reasonably short time of deletion; and thankfully you can have LUKS+NTFS/vfat)

	- I would recommend against encrypted containers while truecrypt, ecryptfs, and even LUKS have the ability to make "virtual encrypted drives", historically there have been ways to data mine these containers

	- using non-journeled filesystems or just [turning off journaling](https://superuser.com/questions/516784/disable-journaling-on-ext4-filesystem-partition) on your secret drive makes recovery of the drive and data analysis more complicated for an attacker. (they can't tell the OS to roll back to an older snapshot and need to attack the method that was used to encrypt a file)

- Don't blindly trust the built in sha512 checking (especially if you are moving files over a potentially unkown network)

- I recommend you use syncthing, git annex, git, unison, rsync or even old school sneakernet to sync files 

	- this will allow you to keep at least 2 duplicates of hashes to ensure data integrity and away from centralized points of failure (if a password gets leaked its because of the site/program or you; no one else)

	- the downside of using things like git and syncthing is that these applications log a complete history of your passwords. (syncthing can have history and snapshots be disabled) but as an upside you can recover passwords via this method. (revert to an older commit or version)

 
